from PyQt5.QtWidgets import *
import sys
from GUI.DemandForecasting import *
from GUI.LoginDialog import *


if __name__ == "__main__":
    app = QApplication(sys.argv)
    loginDialog = LoginDialog()

    username = "Dummy"
    authorizationLevel = 0
    if loginDialog.exec_() == QDialog.Accepted:
        authorizationLevel = loginDialog.authorizationLevel

        username = loginDialog.username
        MainWindow = DemandForecasting(username, authorizationLevel)

        MainWindow.show()
        sys.exit(app.exec_())
