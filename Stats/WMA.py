#weighted moving average function with 3 inputs: sales = lst of sales data; int num_periods; weights = lst of weight data
def WMA (sales, weights):
	# Make sure the weights are normalized.
	weights_sum = sum(weights)
	for i in range(len(weights)):
		weights[i] = weights[i] / weights_sum

	forecast = 0.0
	for i in range(len(weights)): #append product of weights and sales to weighted_sales list
		forecast += weights[i] * sales[i]

	return forecast #return forecast




		