#Mean Error function (bias) with sales and forecast lists as input; sales and forecast must be same size
def bias(sales, forecast):
    #define variable error
    # error = 0

    #for loop runs through any size list size, computes differences between sales and forecast and adds to variable error
    # for i in range(len(sales)):
    #     error += sales[i] - forecast[i]

    #mean error averages the variable error by the length of the sales list
    # mean_error = error/len(sales)

    return sales - forecast

