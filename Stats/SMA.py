#simple moving average function with 2 inputs: sales = lst of sales data; periods = int for moving averaging
def SMA (sales, num_periods):

	reverse_sales = sales[::-1] #reverse sales to simplify sales_sum summation in for loop
	sales_sum = 0 #sum of sales variable
	
	for i in range(num_periods): #sum sales from last-period to last-period - number of periods in question (num_period)
		sales_sum += reverse_sales[i]

	forecast = float (sales_sum / num_periods) #divide sum of sales by number of periods for forecast
	return (forecast) #return forecast





		