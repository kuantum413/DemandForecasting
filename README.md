To run this program:

1. Download and install Python3.5: https://www.python.org/downloads/

2. Download and install PyQt5: https://www.riverbankcomputing.com/software/pyqt/download5

3. Download and install matplotlib: python -m pip install matplotlib

4. Run Main.py from this folder: python Main.py