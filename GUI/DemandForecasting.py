import sys
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import *
from GUI.LoginDialog import *
from GUI.DemandForecastingUI import *
from GUI.GlossaryDialogUI import *
from GUI.ForecastHelpDialogUI import *
from GUI.DataHelpDialogUI import *
from GUI.WeightsDialogUI import *
from GUI.MyMplCanvas import *
import GUI.GuiState as GuiState

from Stats.Bias import *
from Stats.WMA import *
from Stats.SMA import *
from Stats.Exponential_Smoothing import *


class DemandForecasting(QMainWindow, Ui_MainWindow):
    def __init__(self, username, authorizationLevel):
        self.username = username
        self.isLive = False
        # Setup Qt variables
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.startingWindowTitle = self.windowTitle()
        if self.username is not None:
            self.setWindowTitle(self.startingWindowTitle + " - " + self.username)
        # Forecast Help Dialog
        self.ForecastHelpDialog = QDialog()
        self.ForecastHelpDialogUi = Ui_ForecastHelpDialog()
        self.ForecastHelpDialogUi.setupUi(self.ForecastHelpDialog)
        # Data Help Dialog
        self.DataHelpDialog = QDialog()
        self.DataHelpDialogUi = Ui_DataHelpDialog()
        self.DataHelpDialogUi.setupUi(self.DataHelpDialog)
        # Glossary Dialog
        self.glossaryDialog = QDialog()
        self.gloddaryDialogUi = Ui_GlossaryDialog()
        self.gloddaryDialogUi.setupUi(self.glossaryDialog)
        # Weights Dialog
        self.weightsDialog = QDialog()
        self.weightsDialogUi = Ui_WeightsDialog()
        self.weightsDialogUi.setupUi(self.weightsDialog)
        self.weightsTable = self.weightsDialogUi.weightsTable  # Make the weights table a member of this UI for saving
        # Add plots to the GUI
        self.main_widget = QWidget()
        self.salesPlot = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        self.salesPlot.ylabel = "Units"
        self.salesPlot.legendColors = ["r", "b"]
        self.salesPlot.legendLabels = ["Sales", "Forecasts"]
        self.adPlot = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        self.adPlot.ylabel = "Units"
        self.adPlot.legendColors = ["r", None, "g", None, "b"]
        self.adPlot.legendLabels = ["Global MAD =", "NA", "Local MAD =", "NA", "Bias"]

        self.plotsPlaceholder1.setParent(None)  # Remove plots placeholder1 from GUI
        self.plotsPlaceholder2.setParent(None)  # Remove plots placeholder2 from GUI
        self.mplLayout.addWidget(self.salesPlot, 1, 0)
        self.mplLayout.addWidget(self.adPlot, 3, 0)

        # Setup forecasting variables
        self.products = []
        self.product = None
        self.salesPeriodSegs = []
        self.salesSegs = []
        self.fcstPeriodSegs = []
        self.fcstSegs = []
        self.biasPeriodSegs = []
        self.biasSegs = []
        self.globalMAD = None
        self.localMAD = None

        # General connections
        self.tabWidget.currentChanged.connect(self.onTabWidgetCurrentChanged)

        # Toolbar connections
        self.actionChangeUser.triggered.connect(self.onActionChangeUserTriggered)
        self.actionOpen.triggered.connect(self.selectOpenFile)
        self.actionSave.triggered.connect(self.onActionSaveTriggered)
        self.actionExport.triggered.connect(self.onActionExportTriggered)
        self.actionAddProduct.triggered.connect(self.addNewProduct)
        self.actionChangeProduct.triggered.connect(self.selectProduct)
        self.actionRemoveProduct.triggered.connect(self.onActionRemoveProductTriggered)
        self.actionGlossary.triggered.connect(self.openGlossary)
        self.actionHelp.triggered.connect(self.openHelp)

        # Forecast tab connections
        self.checkWeiMovAvg.stateChanged.connect(self.onCheckWeiMovAvgStateChanged)
        self.checkExpSmooth.stateChanged.connect(self.onCheckExpSmoothStateChanged)
        self.checkSimMovAvg.stateChanged.connect(self.onCheckSimMovAvgStateChanged)
        self.weightsButton.clicked.connect(self.openWeights)
        self.forecastButton.clicked.connect(self.onForecastButtonClicked)
        self.numPeriodsEdit.textChanged.connect(self.onNumPeriodsEditTextChanged)
        self.xMinSlider.valueChanged.connect(self.onXMinSliderValueChanged)
        self.xMaxSlider.valueChanged.connect(self.onXMaxSliderValueChanged)

        # Data tab connections
        self.addRowButton.clicked.connect(self.onAddRowButtonClicked)
        self.inputDataTable.cellChanged.connect(self.onInputDataTableCellChanged)
        self.saveButton.clicked.connect(self.onActionSaveTriggered)

        # Load GUI State
        self.settings = QtCore.QSettings("IME580", "DemandForecasting")

        self.loadGui()
        self.updateParameterVisibility()
        self.initializePlots()
        self.setAuthorizationLevel(authorizationLevel)
        self.statusMessage("")

        # Create the undo stack
        # self.undoStack = QUndoStack(self)

        self.isLive = True


    ####################################################################################################################
    # Front End Fuctions
    ####################################################################################################################

    def onActionChangeUserTriggered(self):
        loginDialog = LoginDialog()

        if loginDialog.exec_() == QDialog.Accepted:
            self.setAuthorizationLevel(loginDialog.authorizationLevel)
            self.username = loginDialog.username

            if self.username is not None:
                self.setWindowTitle(self.startingWindowTitle + " - " + self.username)

    def onActionSaveTriggered(self):
        self.saveProductSettings(self.product)
        self.statusMessage("Saving... saved")

    def onActionExportTriggered(self):
        self.selectSaveFile()

    def onXMinSliderValueChanged(self):
        minValue = self.xMinSlider.value()
        maxValue = self.xMaxSlider.value()

        if (minValue >= maxValue):
            maxValue = min(minValue + 1, self.xMaxSlider.maximum())
            self.xMaxSlider.setValue(maxValue)

        self.updatePlotLimits()

    def onXMaxSliderValueChanged(self):
        minValue = self.xMinSlider.value()
        maxValue = self.xMaxSlider.value()

        if (maxValue <= minValue):
            minValue = max(self.xMinSlider.minimum(), maxValue - 1)
            self.xMinSlider.setValue(minValue)

        self.updatePlotLimits()

    def onNumPeriodsEditTextChanged(self):
        self.updateWeightsTable()

    def onForecastButtonClicked(self):
        if self.authorizationLevel >= 1:
            self.forecast()
        else:
            self.authorizationErrorMessage()

    def onInputDataTableCellChanged(self, row, col):
        if self.isLive:
            self.initializePlots()

        if self.tabWidget.currentIndex() == 1 and col == 2:
            self.setDataTableText(row, 3, "BH")

    def onTabWidgetCurrentChanged(self, index):
        if index == 0:
            self.isLive = True
            self.initializePlots()

        if index == 1:
            self.isLive = False

    def onCheckWeiMovAvgStateChanged(self):
        if self.checkWeiMovAvg.isChecked():
            self.checkSimMovAvg.setChecked(False)
            self.checkExpSmooth.setChecked(False)
        self.updateParameterVisibility()

    def onCheckExpSmoothStateChanged(self):
        if self.checkExpSmooth.isChecked():
            self.checkSimMovAvg.setChecked(False)
            self.checkWeiMovAvg.setChecked(False)
        self.updateParameterVisibility()

    def onCheckSimMovAvgStateChanged(self):
        if self.checkSimMovAvg.isChecked():
            self.checkWeiMovAvg.setChecked(False)
            self.checkExpSmooth.setChecked(False)
        self.updateParameterVisibility()

    # Open the weights dialog
    def openWeights(self):
        self.weightsDialog.show()

    # Open the help dialog
    def openHelp(self):
        if self.tabWidget.currentIndex() == 0:
            self.ForecastHelpDialog.show()
        elif self.tabWidget.currentIndex() == 1:
            self.DataHelpDialog.show()

    # Open the glossary dialog
    def openGlossary(self):
        self.glossaryDialog.show()

    # Open a dialog to change products
    def selectProduct(self):
        newProduct = "{New Product}"
        products = self.products + [newProduct]
        product, ok = QInputDialog.getItem(self, "Select Product", "Products", products, editable = False)

        if ok:
            if (product == newProduct):
                self.addNewProduct()
            else:
                self.changeProduct(product)

            self.statusMessage("")

    # Add a new product
    def addNewProduct(self):
        if self.authorizationLevel >= 1:
            newProductName, ok = QInputDialog.getText(self, "New Product", "Name")
            if ok:
                newProductName = newProductName.lstrip().rstrip()
                if newProductName != "":
                    if newProductName not in self.products:
                        self.products = self.products + [newProductName]
                        self.changeProduct(newProductName)
                        self.setDataTableText(0, 0, "1")
                        self.statusMessage("")
                    else:
                        self.dialogMessage("A product with that name already exists.", "Duplicate Product")
                else:
                    self.dialogMessage("A product name must contain more than whitespace.", "Invalid Product Name")
        else:
            self.authorizationErrorMessage()

    # Remove the active product
    def onActionRemoveProductTriggered(self):
        if self.authorizationLevel >= 1:
            self.removeProduct()

            if (len(self.products) > 0):
                self.changeProduct(self.products[0])
            else:
                self.changeProduct(None)
        else:
            self.authorizationErrorMessage()

    # Select a CSV file for loading data
    def selectOpenFile(self):
        if self.authorizationLevel >= 1:
            filter = "CSV (*.csv);; TXT (*.txt)"
            fname = QFileDialog.getOpenFileName(self, "Open File", "C:", filter)[0]
        else:
            self.authorizationErrorMessage()

    # Save a PNG image of the plots
    def selectSaveFile(self):
        if self.authorizationLevel >= 1:
            filter = "PNG (*png)"
            fname = QFileDialog.getSaveFileName(self, "Save File", "C:", filter)[0]
        else:
            self.authorizationErrorMessage()

    # Add a new row to the data table
    def onAddRowButtonClicked(self):
        self.addNewDataTableRow()

    ####################################################################################################################
    # Back End Fuctions
    ####################################################################################################################

    def setAuthorizationLevel(self, level):
        self.authorizationLevel = level

        if self.authorizationLevel < 1:
            self.setDataTableReadOnly(True)
        else:
            self.setDataTableReadOnly(False)

    # Update parameter visibility:
    def updateParameterVisibility(self):
        if self.checkSimMovAvg.isChecked() or self.checkWeiMovAvg.isChecked():
            self.numPeriodsLabel.show()
            self.numPeriodsEdit.show()
        else:
            self.numPeriodsLabel.hide()
            self.numPeriodsEdit.hide()

        if self.checkWeiMovAvg.isChecked():
            self.weightsButton.show()
        else:
            self.weightsButton.hide()

        if self.checkExpSmooth.isChecked():
            self.alphaLabel.show()
            self.editAlpha.show()
        else:
            self.alphaLabel.hide()
            self.editAlpha.hide()

    def updateWeightsTable(self):
        text = self.numPeriodsEdit.text()
        try:
            numPeriods = int(text)
        except ValueError:
            return

        if (self.weightsTable.rowCount() < numPeriods):
            self.weightsTable.setRowCount(numPeriods)

        for i in range(0, self.weightsTable.rowCount()):
            item = self.weightsTable.item(i, 0)
            if (item == None):
                item = QTableWidgetItem()
                self.weightsTable.setItem(i, 0, item)

        for i in range(0, numPeriods):
            item = self.weightsTable.item(i, 0)
            text = item.text()
            if text == "":
                item.setText("1")

    # Add a new row, full of items, to the end of the data table
    def addNewDataTableRow(self):
        self.inputDataTable.insertRow(self.inputDataTable.rowCount())
        newRow = self.inputDataTable.rowCount() - 1
        for col in range(0, self.inputDataTable.columnCount()):
            item = QTableWidgetItem()
            self.inputDataTable.setItem(newRow, col, item)

    # Forecast based on current GUI params
    def forecast(self):
        # Find the current row in the table. It should have a period value and a forecast value.
        nRows = self.inputDataTable.rowCount()
        currentRow = None
        for row in reversed(range(0, nRows)):
            currentPeriod = None
            currentForecast = None

            # Check for a valid period value
            text = self.getDataTableText(row, 0)
            try:
                currentPeriod = int(text)
            except ValueError:
                continue

            # Check for valid forecast value
            text = self.getDataTableText(row, 2)
            try:
                currentForecast = float(text)
            except ValueError:
                pass

            # If both values are valid stop searching
            if currentPeriod is not None:
                currentRow = row
                break

        # If you couldn't find a period
        if currentPeriod is None:
            self.dialogMessage("There is no period in your data table from which to start.",
                                        "Missing Start Period")
            return

        # Check if there is existing sales data
        existingSales = None
        text = self.getDataTableText(currentRow, 1)
        try:
            existingSales = float(text)
        except ValueError:
            pass

        # See if there is a current sales value being input
        text = self.editCurrentSales.text()
        try:
            currentSales = float(text)

            if existingSales is not None:
                msg = "There seems to be existing sales data already in your data table. Do you want to override it?"
                choice = QMessageBox.question(self, "Sales Conflict", msg,
                                              QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel,
                                              QMessageBox.Cancel)
                if (choice == QMessageBox.Cancel):
                    return
                elif (choice == QMessageBox.Yes):
                    pass
                else:
                    currentSales = existingSales
            self.setDataTableText(currentRow, 1, str(currentSales))
        except ValueError:
            if existingSales is None:
                self.dialogMessage("Either input a value in current sales, or update this period's sales value in the data table.",
                                   "Missing Sales Value")
                return
            else:
                currentSales = existingSales

        # Check if we are calculating via moving average
        method = ""
        if self.checkSimMovAvg.isChecked():
            method = "SMA"
            forecast = self.calcSimpleMovingAverage(currentRow, currentSales)
            if forecast is None:
                return
        elif self.checkWeiMovAvg.isChecked():
            method = "WMA"
            forecast = self.calcWeightedMovingAverage(currentRow, currentSales)
            if forecast is None:
                return
        elif self.checkExpSmooth.isChecked():
            # There must be an existing forecast for this method.
            if currentForecast is None:
                self.dialogMessage("There must be a valid forecast for period %s." % str(currentPeriod),
                                   "Missing Forecast")
                return

            method = "ES"
            forecast = self.calcExpSmooth(currentRow, currentSales, currentForecast)
            if forecast is None:
                return
        else:
            return

        # Make sure there is an empty row for the new forecast
        if currentRow == self.inputDataTable.rowCount() - 1:
            self.addNewDataTableRow()
        newRow = currentRow + 1
        self.setDataTableText(newRow, 0, str(currentPeriod + 1))
        self.setDataTableText(newRow, 2, str(forecast))
        self.setDataTableText(newRow, 3, method)

        self.editCurrentSales.setText("")
        self.statusMessage("The forecast for period " + str(currentPeriod + 1) + " is " + str(forecast))

    # Calculate the exponential smoothing forecast
    def calcExpSmooth(self, currentRow, currentSales, currentForecast):
        text = self.editAlpha.text()
        try:
            alpha = float(text)
        except ValueError:
            self.dialogMessage("Alpha must be a number between 0.0 and 1.0", "Invalid Alpha Value")
            return None

        if alpha < 0.0 or alpha > 1.0:
            self.dialogMessage("Alpha must be a number between 0.0 and 1.0", "Invalid Alpha Value")
            return None

        return round(Exponential_Smoothing(currentSales, alpha, currentForecast), 2)

    # Calculate the weighted moving average forecast
    def calcWeightedMovingAverage(self, currentRow, currentSales):
        # Get the current number of periods
        text = self.numPeriodsEdit.text()
        try:
            numPeriods = int(text)
        except ValueError:
            self.dialogMessage("N-Periods must contain an integer value.", "Invalid N-Periods Value")
            return None

        # Make sure there is enough sales data
        sales = []
        if (numPeriods > currentRow + 1):
            self.dialogMessage("Your N-Periods value is too large. There is not enough sales data to support that many weighted periods.",
                              "Insufficient Sales Data")
            return None
        for period in reversed(range(0, numPeriods)):
            row = currentRow - period
            text = self.getDataTableText(row, 1)
            try:
                salesValue = float(text)
                sales.append(salesValue)
            except ValueError:
                self.dialogMessage("You need a valid sales value in row " + str(row + 1) + ".",
                                   "Invalid Sales Value")
                return None

        # Grab all of the weights
        weights = []
        for row in range(0, numPeriods):
            item = self.weightsTable.item(row, 0)
            if item is None:
                self.dialogMessage("Please adjust your weight for the " + str(row) + "th period.",
                                   "Invalid Weight Value")
                return

            text = item.text()
            try:
                weight = float(text)
                weights.append(weight)
            except ValueError:
                self.dialogMessage("The weight for the " + str(row) + "th period must be a number",
                                   "Invalid Weight Value")
                return

        return round(WMA(sales, weights), 2)

    # Calculate a simple moving average forecast
    def calcSimpleMovingAverage(self, currentRow, currentSales):
        # Get the current number of periods
        text = self.numPeriodsEdit.text()
        try:
            numPeriods = int(text)
        except ValueError:
            self.dialogMessage("N-Periods must contain an integer value.", "Invalid N-Periods Value")
            return None

        # Make sure there is enough sales data
        sales = []
        if (numPeriods > currentRow + 1):
            self.dialogMessage(
                "Your N-Periods value is too large. There is not enough sales data to support that many weighted periods.",
                "Insufficient Sales Data")
            return None
        for period in reversed(range(0, numPeriods)):
            row = currentRow - period
            text = self.getDataTableText(row, 1)
            try:
                salesValue = float(text)
                sales.append(salesValue)
            except ValueError:
                self.dialogMessage("You need a valid sales value in row " + str(row + 1) + ".",
                                   "Invalid Sales Value")
                return None

        return round(SMA(sales, numPeriods), 2)

    # Update the MAD
    def updateMAD(self):
        self.globalMAD = None
        self.localMAD = None

        if (self.salesPlot.xMax is None or self.salesPlot.xMin is None):
            return

        minSliderValue = self.xMinSlider.value()
        maxSliderValue = self.xMaxSlider.value()

        minPerc = minSliderValue / (self.xMinSlider.maximum() - self.xMinSlider.minimum())
        maxPerc = maxSliderValue / (self.xMaxSlider.maximum() - self.xMaxSlider.minimum())
        valueRange = self.salesPlot.xMax - self.salesPlot.xMin

        minValue = self.salesPlot.xMin + minPerc * valueRange
        maxValue = self.salesPlot.xMin + maxPerc * valueRange

        globalADSum = 0.0
        globalADCount = 0
        localADSum = 0.0
        localADCount = 0
        for i in range(0, len(self.biasSegs)):
            for j in range(0, len(self.biasSegs[i])):
                period = self.biasPeriodSegs[i][j]
                bias = self.biasSegs[i][j]

                globalADSum += abs(bias)
                globalADCount += 1
                if (period >= minValue and period <= maxValue):
                    localADSum += abs(bias)
                    localADCount += 1
        if globalADCount > 0:
            globalMAD = globalADSum / globalADCount
        else:
            globalMAD = None
        if localADCount > 0:
            localMAD = localADSum / localADCount
        else:
            localMAD = None

        self.globalMAD = globalMAD
        self.localMAD = localMAD

    # Update the plots
    def initializePlots(self):
        self.updateSegments()
        self.updatePlotSegments()
        self.updateMAD()
        self.updatePlotLegends()
        self.onXMinSliderValueChanged()
        self.onXMaxSliderValueChanged()

        self.salesPlot.draw()
        self.adPlot.draw()

    # Update the plot limits to match the slider values
    def updatePlotLimits(self):
        minValue = self.xMinSlider.value()
        maxValue = self.xMaxSlider.value()

        if (self.salesPlot.xMin is None or self.salesPlot.xMax is None):
            return

        minPerc = minValue / (self.xMinSlider.maximum() - self.xMinSlider.minimum())
        maxPerc = maxValue / (self.xMaxSlider.maximum() - self.xMaxSlider.minimum())
        range = self.salesPlot.xMax - self.salesPlot.xMin
        minValue = self.salesPlot.xMin + minPerc * range
        maxValue = self.salesPlot.xMin + maxPerc * range

        self.updateMAD()
        self.updatePlotSegments()
        self.salesPlot.setLimits(minValue, maxValue)
        self.adPlot.setLimits(minValue, maxValue)
        self.updatePlotLegends()

        self.salesPlot.draw()
        self.adPlot.draw()

    # Update the plot legends
    def updatePlotLegends(self):
        if isinstance(self.globalMAD, float):
            self.adPlot.legendLabels[1] = "%.2f" % self.globalMAD
        else:
            self.adPlot.legendLabels[1] = "NA"
        if isinstance(self.localMAD, float):
            self.adPlot.legendLabels[3] = "%.2f" % self.localMAD
        else:
            self.adPlot.legendLabels[3] = "NA"

        self.salesPlot.legend()
        self.adPlot.legend()

    # Update the segments used in plots
    def updateSegments(self):
        self.salesPeriodSegs = []
        self.salesSegs = []
        self.fcstPeriodSegs = []
        self.fcstSegs = []
        self.biasPeriodSegs = []
        self.biasSegs = []
        nRows = self.inputDataTable.rowCount()

        # Get data from the table
        salesPeriodSeg = []
        salesSeg = []
        fcstPeriodSeg = []
        fcstSeg = []
        biasPeriodSeg = []
        biasSeg = []
        for row in range(0, nRows):
            # Make sure there is a valid period
            validPeriod = False
            text = self.getDataTableText(row, 0)
            try:
                period = float(text)
                validPeriod = True
            except ValueError:
                pass

            # Make sure there is a valid sales value
            validSales = False
            text = self.getDataTableText(row, 1)
            try:
                sales = float(text)
                validSales = True
            except ValueError:
                pass

            # If there is a valid period and valid sale add them to the current segment
            if validPeriod and validSales:
                salesPeriodSeg.append(period)
                salesSeg.append(sales)
            else:
                if len(salesPeriodSeg) > 0 and len(salesSeg) > 0:
                    self.salesPeriodSegs.append(salesPeriodSeg)
                    self.salesSegs.append(salesSeg)
                salesPeriodSeg = []
                salesSeg = []

            # Make sure there is a valid forecast value
            validFcst = False
            text = self.getDataTableText(row, 2)
            try:
                fcst = float(text)
                validFcst = True
            except ValueError:
                pass

            # If there is a valid period and valid forecast add them to the current segment
            if validPeriod and validFcst:
                fcstPeriodSeg.append(period)
                fcstSeg.append(fcst)
            else:
                if len(fcstPeriodSeg) > 0 and len(fcstSeg) > 0:
                    self.fcstPeriodSegs.append(fcstPeriodSeg)
                    self.fcstSegs.append(fcstSeg)
                fcstPeriodSeg = []
                fcstSeg = []

            # If there is a valid period, sales, and forecast add the bias to current segment
            if validPeriod and validSales and validFcst:
                biasValue = bias(sales, fcst)
                biasPeriodSeg.append(period)
                biasSeg.append(biasValue)
            else:
                if len(biasPeriodSeg) > 0 and len(biasSeg) > 0:
                    self.biasPeriodSegs.append(biasPeriodSeg)
                    self.biasSegs.append(biasSeg)
                biasPeriodSeg = []
                biasSeg = []

        # Add the last segments if they exist
        if len(salesPeriodSeg) > 0 and len(salesSeg) > 0:
            self.salesPeriodSegs.append(salesPeriodSeg)
            self.salesSegs.append(salesSeg)
        if len(fcstPeriodSeg) > 0 and len(fcstSeg) > 0:
            self.fcstPeriodSegs.append(fcstPeriodSeg)
            self.fcstSegs.append(fcstSeg)
        if len(biasPeriodSeg) > 0 and len(biasSeg) > 0:
            self.biasPeriodSegs.append(biasPeriodSeg)
            self.biasSegs.append(biasSeg)

    # Send current segments to the plots
    def updatePlotSegments(self):
        # Populate the sales plot
        self.salesPlot.clear_figure()
        for i in range(0, len(self.salesPeriodSegs)):
            periodSeg = self.salesPeriodSegs[i]
            salesSeg = self.salesSegs[i]
            self.salesPlot.update_figure(periodSeg, salesSeg, 'r')

        # Populate the forecasts plot
        for i in range(0, len(self.fcstPeriodSegs)):
            periodSeg = self.fcstPeriodSegs[i]
            fcstSeg = self.fcstSegs[i]
            self.salesPlot.update_figure(periodSeg, fcstSeg, 'b')

        # Populate the bias plot
        self.adPlot.clear_figure()
        for i in range(0, len(self.biasPeriodSegs)):
            periodSeg = self.biasPeriodSegs[i]
            biasSeg = self.biasSegs[i]
            self.adPlot.update_figure(periodSeg, biasSeg, 'b')
        if self.globalMAD is not None:
            if self.salesPlot.xMin is not None and self.salesPlot.xMax is not None:
                x = [self.salesPlot.xMin, self.salesPlot.xMax]
                y = [self.globalMAD, self.globalMAD]
                self.adPlot.update_figure(x, y, 'r')

                y = [self.localMAD, self.localMAD]
                self.adPlot.update_figure(x, y, 'g')

    # Change the active product
    def changeProduct(self, product):
        # Save current product settings
        self.saveProductSettings(self.product)

        # Load new product settings
        self.product = product
        self.loadProductSettings(self.product)

    # Remove the active product
    def removeProduct(self):
        if (self.product is not None and self.product in self.products):
            msg = "Are you sure you want to delete " + self.product + "?"
            choice = QMessageBox.question(self, "Product Deletion", msg, QMessageBox.Yes | QMessageBox.No,
                                          QMessageBox.No)

            if (choice == QMessageBox.Yes):
                removedProduct = self.product
                self.products.remove(removedProduct)
                self.clearProductSettings(removedProduct)
                self.product = None

                self.statusMessage("Removed " + removedProduct)

    # Load new data into the table
    def loadSalesData(self, periods, sales, forecasts, methods):
        nRows = self.inputDataTable.rowCount()
        nCols = self.inputDataTable.columnCount()
        maxRows = max(len(periods), len(sales), len(forecasts), len(methods))
        lists = [periods, sales, forecasts, methods]

        for row in range(0, maxRows):
            if (row > nRows - 1):
                self.inputDataTable.insertRow(row)
                for col in range(0, nCols):
                    item = QTableWidgetItem()
                    self.inputDataTable.setItem(row, col, item)

                for col in range(0, nCols):
                    if len(lists[col]) > row:
                        self.setDataTableText(row, col, str((lists[col])[row]))

    # Update the status message at the bottom of the GUI
    def statusMessage(self, message):
        statusMessage = ""
        if (self.product is not None):
            statusMessage += self.product
        statusMessage += ": " + message
        self.statusbar.showMessage(statusMessage)

    # Get a settings object for a given product
    def getProductSettings(self, product):
        settings = QtCore.QSettings("IME580", "DemandForecasting:" + product)
        return settings

    # Save the settings for a given product
    def saveProductSettings(self, product):
        if product is not None:
            settings = self.getProductSettings(product)
            GuiState.guisave(self, settings)

    # Load the settings for a given product
    def loadProductSettings(self, product):
        if product is not None:
            settings = self.getProductSettings(product)

            # Turn off table updates for performance reasons
            self.isLive = False
            GuiState.guirestore(self, settings)
            # And update the plots manually after turning table updates back on
            self.isLive = True
        self.productLabel.setText("Product: " + str(self.product))
        self.productLabel2.setText("Product: " + str(self.product))
        self.initializePlots()

    # Clear the settings for a given product
    def clearProductSettings(self, product):
        settings = self.getProductSettings(product)
        settings.clear()

    # Load the last state of the GUI
    def loadGui(self):
        if (self.settings.value("activeProduct") is not None):
            self.product = self.settings.value("activeProduct")
            self.loadProductSettings(self.product)

        if (self.settings.value("numProducts") is not None):
            for i in range(0, self.settings.value("numProducts")):
                productKey = "product" + str(i)
                if (self.settings.value(productKey) is not None):
                    self.products.append(self.settings.value(productKey))

    # Save the last state of the GUI
    def saveGui(self):
        self.settings.setValue("activeProduct", self.product)
        self.saveProductSettings(self.product)

        self.settings.setValue("numProducts", len(self.products))
        for i, product in enumerate(self.products):
            self.settings.setValue("product" + str(i), product)

    # Set the data table to be read only
    def setDataTableReadOnly(self, readOnly):
        if readOnly:
            self.inputDataTable.setEditTriggers(QAbstractItemView.NoEditTriggers)
        else:
            self.inputDataTable.setEditTriggers(QAbstractItemView.AllEditTriggers)

    # Set the

    # Set the text of a table item
    def setDataTableText(self, row, col, text):
        item = self.inputDataTable.item(row, col)
        if isinstance(item, QTableWidgetItem):
            item.setText(text)
            return True
        else:
            nRows = self.inputDataTable.rowCount()
            if nRows < row + 1:
                self.inputDataTable.setRowCount(row + 1)

            item = QTableWidgetItem()
            item.setText(text)
            self.inputDataTable.setItem(row, col, item)
            return True

        return False

    # Get the text of a table item
    def getDataTableText(self, row, col):
        item = self.inputDataTable.item(row, col)
        if isinstance(item, QTableWidgetItem):
            text = item.text()
            return text
        return ""

    # Give an authorization error to the user
    def authorizationErrorMessage(self):
        msg = "You are not authorized to perform this task"
        title = "Authorization Error"
        self.dialogMessage(msg, title)

    # Give a dialog message to the user
    def dialogMessage(self, msg, title):
        choice = QMessageBox.question(self, title, msg, QMessageBox.Ok)
        return choice

    # Override the default resize event
    def resizeEvent(self, resizeEvent):
        self.updatePlotLimits()

    # Override the native close event
    def closeEvent(self, event):
        msg = "Are you sure you want to close the program?"
        choice = QMessageBox.question(self, "Product Deletion", msg, QMessageBox.Yes | QMessageBox.No,
                                      QMessageBox.No)

        if (choice == QMessageBox.Yes):
            self.saveGui()
            event.accept()
        else:
            event.ignore()
