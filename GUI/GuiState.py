#===================================================================

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import inspect

#===================================================================
# save "ui" controls and values to registry "setting"
# currently only handles comboboxes editlines & checkboxes
# ui = qmainwindow object
# settings = qsettings object
#===================================================================


def guisave(ui, settings):

    #for child in ui.children():  # works like getmembers, but because it traverses the hierarachy, you would have to call guisave recursively to traverse down the tree

    for name, obj in inspect.getmembers(ui):

        # if isinstance(obj, QLabel):
        #     name = obj.objectName()
        #     text = obj.text()
        #     settings.setValue(name, text)

        #if type(obj) is QComboBox:  # this works similar to isinstance, but missed some field... not sure why?
        if isinstance(obj, QComboBox):
            name   = obj.objectName()      # get combobox name
            index  = obj.currentIndex()    # get current index from combobox
            text   = obj.itemText(index)   # get the text for current index
            settings.setValue(name, text)   # save combobox selection to registry

        if isinstance(obj, QLineEdit):
            name = obj.objectName()
            value = obj.text()
            settings.setValue(name, value)    # save ui values, so they can be restored next time

        if isinstance(obj, QCheckBox):
            name = obj.objectName()
            state = obj.checkState()
            settings.setValue(name, state)

        if isinstance(obj, QTableWidget):
            nRows = obj.rowCount()
            nCols = obj.columnCount()
            settings.setValue(name + ".rowCount", nRows)
            settings.setValue(name + ".columnCount", nCols)

            for row in range(0, nRows):
                for col in range(0, nCols):
                    item = obj.item(row, col)
                    if item is not None:
                        text = item.text()
                    else:
                        text = ""
                    settings.setValue(name + "[" + str(row) + "," + str(col) + "]", text)



#===================================================================
# restore "ui" controls with values stored in registry "settings"
# currently only handles comboboxes, editlines &checkboxes
# ui = QMainWindow object
# settings = QSettings object
#===================================================================

def guirestore(ui, settings):

    for name, obj in inspect.getmembers(ui):
        #
        # if isinstance(obj, QLabel):
        #     name = obj.objectName()
        #     value = settings.value(name)
        #     if value is not None:
        #         obj.setText(str(value))

        if isinstance(obj, QComboBox):
            index  = obj.currentIndex()    # get current region from combobox
            #text   = obj.itemText(index)   # get the text for new selected index
            name   = obj.objectName()

            value = str(settings.value(name))

            if value == "":
                continue

            index = obj.findText(value)   # get the corresponding index for specified string in combobox

            if index == -1:  # add to list if not found
                obj.insertItems(0,[value])
                index = obj.findText(value)
                obj.setCurrentIndex(index)
            else:
                obj.setCurrentIndex(index)   # preselect a combobox value by index

        if isinstance(obj, QLineEdit):
            name = obj.objectName()
            value = settings.value(name)  # get stored value from registry
            if value is not None:
                obj.setText(str(value))  # restore lineEditFile

        if isinstance(obj, QCheckBox):
            name = obj.objectName()
            value = settings.value(name)   # get stored value from registry
            if value != None:
                obj.setChecked(value)   # restore checkbox

        if isinstance(obj, QTableWidget):
            # Clear out any existing content
            obj.clearContents()

            name = obj.objectName()
            nRows = settings.value(name + ".rowCount")
            nCols = settings.value(name + ".columnCount")

            if not isinstance(nRows, int) or not isinstance(nCols, int):
                continue

            # Build out the table
            obj.setRowCount(nRows)
            obj.setColumnCount(nCols)
            for row in range(0, nRows):
                for col in range(0, nCols):
                    item = QTableWidgetItem()
                    obj.setItem(row, col, item)

            # Populate with items
            for row in range(0, nRows):
                for col in range(0, nCols):
                    value = settings.value(name + "[" + str(row) + "," + str(col) + "]")

                    if value is not None:
                        obj.item(row, col).setText(value)

        #if isinstance(obj, QRadioButton):