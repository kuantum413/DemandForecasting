import sys
import random
import matplotlib
matplotlib.use("Qt5Agg")
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QMenu, QVBoxLayout, QSizePolicy, QMessageBox, QWidget
from numpy import arange, sin, pi
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.font_manager import FontProperties


class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi, facecolor="white")
        self.axes = self.fig.add_subplot(111)
        self.axes.set_axis_bgcolor((239 / 255, 240 / 255, 241 / 255))
        self.fontP = FontProperties()
        self.fontP.set_size("small")
        # Make room for the legend
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)
        self.xlabel = ""
        self.ylabel = ""
        self.title = ""
        self.legendColors = []
        self.legendLabels = []
        self.x = []
        self.y = []
        self.xMin = None
        self.xMax = None

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

"""A canvas that updates itself every second with a new plot."""
class MyDynamicMplCanvas(MyMplCanvas):
    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        self.axes.hold(True)

    def update_figure(self, x, y, col):
        self.axes.plot(x, y, col)

        if self.xMin is None:
            self.xMin = x[0]
        if self.xMax is None:
            self.xMax = x[0]
        for xVal in x:
            if xVal < self.xMin:
                self.xMin = xVal
            elif xVal > self.xMax:
                self.xMax = xVal

    def clear_figure(self):
        self.axes.clear()
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabel)
        self.axes.set_title(self.title)
        self.xMin = None
        self.xMax = None
        self.legendRoom = False

    def legend(self):
        legendArtists = []
        for color in self.legendColors:
            if color is None:
                artist = matplotlib.patches.Rectangle((0, 0), 1, 1, fill=False, edgecolor='none', visible=False)
            else:
                artist = self.axes.plot(0, 0, color)[0]
            legendArtists.append(artist)

        self.axes.legend(legendArtists, self.legendLabels, loc="center left", bbox_to_anchor=(1.0, 0.5), prop = self.fontP)

    def setLimits(self, left, right):
        self.axes.set_xlim(left=left, right=right)