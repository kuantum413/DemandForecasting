# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'WeightsDialog.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WeightsDialog(object):
    def setupUi(self, WeightsDialog):
        WeightsDialog.setObjectName("WeightsDialog")
        WeightsDialog.resize(406, 320)
        self.verticalLayout = QtWidgets.QVBoxLayout(WeightsDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.weightsTable = QtWidgets.QTableWidget(WeightsDialog)
        self.weightsTable.setObjectName("weightsTable")
        self.weightsTable.setColumnCount(1)
        self.weightsTable.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.weightsTable.setHorizontalHeaderItem(0, item)
        self.verticalLayout.addWidget(self.weightsTable)
        self.label = QtWidgets.QLabel(WeightsDialog)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.buttonBox = QtWidgets.QDialogButtonBox(WeightsDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(WeightsDialog)
        self.buttonBox.accepted.connect(WeightsDialog.accept)
        self.buttonBox.rejected.connect(WeightsDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(WeightsDialog)

    def retranslateUi(self, WeightsDialog):
        _translate = QtCore.QCoreApplication.translate
        WeightsDialog.setWindowTitle(_translate("WeightsDialog", "Moving Average Weights"))
        item = self.weightsTable.horizontalHeaderItem(0)
        item.setText(_translate("WeightsDialog", "Weight"))
        self.label.setToolTip(_translate("WeightsDialog", "For example, if the weights for periods 1, 2, and 3 are 1, 3, and 1 respectively, then period 2 has three times the weight of periods 1 and 3."))
        self.label.setText(_translate("WeightsDialog", "Note: All weights in this table are considered relative to each other in calculation."))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WeightsDialog = QtWidgets.QDialog()
    ui = Ui_WeightsDialog()
    ui.setupUi(WeightsDialog)
    WeightsDialog.show()
    sys.exit(app.exec_())

