from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import *
import sys
from GUI.LoginDialogUI import *


class LoginDialog(QDialog):

    def __init__(self):
        QDialog.__init__(self)
        self.loginDialogUi = Ui_LoginDialog()
        self.loginDialogUi.setupUi(self)
        self.username = None
        self.password = None
        self.authorizationLevel = -2
        self.users0 = {"Student":  "IME"}
        self.users1 = {"Keenan":  "Keenan", "Derrick":  "Derrick", "Dr. P":  "Dr. P"}
        self.users = [self.users0, self.users1]

    def accept(self):
        username = self.loginDialogUi.editUsername.text()
        password = self.loginDialogUi.editPassword.text()

        # Check for a valid username and password combo, noting the user level
        for userLevel, users in enumerate(self.users):

            for user_username, user_password in users.items():
                if user_username == username and user_password == password:
                    self.username = username
                    self.password = password
                    self.authorizationLevel = userLevel
                    break
            if self.authorizationLevel > -1:
                break

        if self.authorizationLevel > -1:
            QDialog.accept(self)
        else:
            title = "Invalid Username or Password"
            msg = "Pleae enter a valid username or password"
            choice = QMessageBox.question(self, title, msg, QMessageBox.Ok)
