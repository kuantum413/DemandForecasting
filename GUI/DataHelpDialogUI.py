# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DataHelpDialog.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DataHelpDialog(object):
    def setupUi(self, DataHelpDialog):
        DataHelpDialog.setObjectName("DataHelpDialog")
        DataHelpDialog.resize(521, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(DataHelpDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textEdit = QtWidgets.QTextEdit(DataHelpDialog)
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        self.buttonBox = QtWidgets.QDialogButtonBox(DataHelpDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(DataHelpDialog)
        self.buttonBox.accepted.connect(DataHelpDialog.accept)
        self.buttonBox.rejected.connect(DataHelpDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(DataHelpDialog)

    def retranslateUi(self, DataHelpDialog):
        _translate = QtCore.QCoreApplication.translate
        DataHelpDialog.setWindowTitle(_translate("DataHelpDialog", "Help - Data"))
        self.textEdit.setHtml(_translate("DataHelpDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">NOTE</span><span style=\" font-size:8pt;\">: The forecast functions (Simple Moving Average, Weighted Moving Average and Exponential Smoothing) will forecast after the most recent non-blank period. Always check to make sure data is populated correctly within the worksheet prior to clicking “Forecast” under the Forecast tab. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">  </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">To Alter Data in the Worksheet:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">1.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Double-click inside the cell requiring alteration </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">a.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">A cursor will appear inside the cell and the value will become highlighted </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">2.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Change the value inside the cell </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">3.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Click the “Save” button at the bottom right corner to save the data </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">  </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">To Save a Worksheet’s Data:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">1.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Click the “Save” button at the bottom right corner of the “Data” tab </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">a.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">The data will be saved under the specific product displayed in red at the top of the tab </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">  </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">To Add a Row to the Worksheet:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">1.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Click “+Row” button at the bottom left corner of the “Data” tab </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">a.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">A new row will be added to the worksheet </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">  </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">To Erase a Period’s Data:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">1.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Double-click inside the cells containing the data that needs to be erased </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">a.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">A cursor will appear inside the cell </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">2.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Erase data by pressing the “Back” or “Delete” button on the keyboard </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">3.</span><span style=\" font-family:\'Times New Roman\'; font-size:7pt;\">       </span><span style=\" font-size:8pt;\">Repeat steps 1 and 2 for each column of the period which needs to be erased </span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DataHelpDialog = QtWidgets.QDialog()
    ui = Ui_DataHelpDialog()
    ui.setupUi(DataHelpDialog)
    DataHelpDialog.show()
    sys.exit(app.exec_())

