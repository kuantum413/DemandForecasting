# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GlossaryDialog.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GlossaryDialog(object):
    def setupUi(self, GlossaryDialog):
        GlossaryDialog.setObjectName("GlossaryDialog")
        GlossaryDialog.resize(619, 321)
        self.verticalLayout = QtWidgets.QVBoxLayout(GlossaryDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textEdit = QtWidgets.QTextEdit(GlossaryDialog)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        self.buttonBox = QtWidgets.QDialogButtonBox(GlossaryDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(GlossaryDialog)
        self.buttonBox.accepted.connect(GlossaryDialog.accept)
        self.buttonBox.rejected.connect(GlossaryDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(GlossaryDialog)

    def retranslateUi(self, GlossaryDialog):
        _translate = QtCore.QCoreApplication.translate
        GlossaryDialog.setWindowTitle(_translate("GlossaryDialog", "Glossary"))
        self.textEdit.setHtml(_translate("GlossaryDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; text-decoration: underline;\">Menu Options:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">File: </span><span style=\" font-size:8pt;\">Open and load a specific product’s CSV file into the Data tab, or save changes to a product’s Data. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Edit</span><span style=\" font-size:8pt;\">: Undo last action. Redo last action. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Product</span><span style=\" font-size:8pt;\">: Change, add or remove a product from the program. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">  </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; text-decoration: underline;\">Forecast Tab:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Simple Moving Average</span><span style=\" font-size:8pt;\">:  Forecasts the next period’s demand by averaging historical sales data over a selected number of past periods (N-periods). Equal weights are applied to each forecast period. Addition of new demand data (sales corresponding to a recent period) shifts the forecast to the most recent number of selected periods (N-periods). Best used when historical demand is consistent with expectations and exceeds the selected number of past periods (N-periods). </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Weighted Moving Average</span><span style=\" font-size:8pt;\">:  Forecasts the next period’s demand by averaging historical sales data over a selected number of past periods (N-periods). Different weights are entered under the “Adjust Weights” menu to adjust for inconsistencies resulting from outliers. Addition of new demand data (sales corresponding to a recent period) shifts the forecast to the most recent number of selected periods (N-periods) and requires weights to be adjusted accordingly. Best used when historical demand data is inconsistent with expectations and exceeds the selected number of past periods (N-periods). </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Exponential Smoothing</span><span style=\" font-size:8pt;\">:  Forecasts the next period’s demand by accounting for the proportion of error incorporated into a forecast. Error is represented by an exponential smoothing constant (Alpha), ranging between 0 and 1, which can be entered in the box labeled “Alpha”. Values of Alpha less than 0.3 are used when historical data shows volatility, while values of Alpha greater than 0.8 are used when historical data is trending (either positively or negatively).  Best used when little historical demand is available. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">N-Periods</span><span style=\" font-size:8pt;\">: Utilized with Simple Moving Average and Weighted Moving Average. Designates the number of periods historical sales data should be averaged over. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Adjust Weights</span><span style=\" font-size:8pt;\">: Utilized with Weighted Moving Average. Applies separate weights to each period’s historical sales data. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Alpha</span><span style=\" font-size:8pt;\">: Exponential smoothing constant utilized with Exponential Smoothing. Represents the proportion of error incorporated into a forecast. Values of Alpha must range from 0 to 1: values of Alpha less than 0.3 are used for historical data with volatility, while values of Alpha greater than 0.8 are used when historical data is trending (either positively or negatively). </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Mean Error (Bias)</span><span style=\" font-size:8pt;\">: Measurement of forecast accuracy over a selected number of periods (N-Periods). Monitors deviation between a period’s sales and the same period’s forecast and is represented by the following formula: </span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\"file:///C:\\Users\\kreim\\AppData\\Local\\Temp\\msohtmlclip1\\01\\clip_image001.png\" width=\"455\" height=\"60\" /><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Mean Absolute Deviation (MAD)</span><span style=\" font-size:8pt;\">: Measurement of forecast error magnitudes over a selected number of periods (N-periods). Monitors the absolute deviation between a period’s sales and the same period’s forecast and is represented by the following formula: </span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\"file:///C:\\Users\\kreim\\AppData\\Local\\Temp\\msohtmlclip1\\01\\clip_image002.png\" width=\"541\" height=\"43\" /><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Current Sales</span><span style=\" font-size:8pt;\">: Utilized to update the program with the most recent sales data. The last period’s sales are entered into the “Current Sales” box prior to “Forecast” being selected. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Forecast</span><span style=\" font-size:8pt;\">: Executes the selected algorithm (Simple Moving Average, Weighted Moving Average or Exponential Smoothing) with the specified criteria (N-Periods, Adjust Weights, Alpha). Forecast prediction is displayed under the “Forecast” push button. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Local MAD</span><span style=\" font-size:8pt;\">: Mean Absolute Deviation (MAD) of the periods currently displayed on the plots – this value will change as the sliders are adjusted to change the range of data displayed. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Global MAD</span><span style=\" font-size:8pt;\">: Mean Absolute Deviation (MAD) of all data periods contained within the “Data” tab – this value will not change as the sliders are adjusted. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\"> </span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; text-decoration: underline;\">Data Tab:</span><span style=\" font-size:8pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Period</span><span style=\" font-size:8pt;\">: Data column representing a “time bucket” of information corresponding to a period of time designated by the organization for product turnover (days, weeks, months, etc.). </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Sales</span><span style=\" font-size:8pt;\">: Data column representing actual sales demand for a given period. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Forecast</span><span style=\" font-size:8pt;\">: Data column representing the projected forecast for a given period calculated through a specific algorithm (Simple Moving Average, Weighted Moving Average or Exponential Smoothing).  </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">Method</span><span style=\" font-size:8pt;\">: Data column representing the specific algorithm used to calculate the forecast for a given period. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">+Row</span><span style=\" font-size:8pt;\">: Adds a row to the current products data sheet. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">  </span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    GlossaryDialog = QtWidgets.QDialog()
    ui = Ui_GlossaryDialog()
    ui.setupUi(GlossaryDialog)
    GlossaryDialog.show()
    sys.exit(app.exec_())

